

Pod::Spec.new do |s|

  s.name         = "TMWVKPaManageSDK"
  s.version      = "0.0.4"
  s.summary      = "TM web"
  s.homepage     = "https://www.360tianma.com"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "renxukui" => "renxukui@360tianma.com" }
  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://gitee.com/tmgc/tmwvkpa-manage-spec.git", :tag => s.version}

  s.source_files = 'TMFramework/TMWVKPaManageSDK.framework/Headers/*.{h}'
  s.ios.vendored_frameworks = 'TMFramework/TMWVKPaManageSDK.framework'
  s.requires_arc = true
  s.dependency "TMSDK"
  s.dependency "TMUserCenter"
s.dependency "TMWebViewKitSDK"
s.dependency "TMPaySDK"

s.xcconfig = {
  'VALID_ARCHS' =>  'armv7 x86_64 arm64',
}
end
