
//
//  TMWVKPaWebViewController.h
//  TMWVKPaManageSDK
//
//  Created by rxk on 2022/1/24.
//  Copyright © 2022 Tianma. All rights reserved.
//

#import <TMWebViewKitSDK/TMWebViewKitSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface TMWVKPaWebViewController : TMWVKWebViewController

@end

NS_ASSUME_NONNULL_END
