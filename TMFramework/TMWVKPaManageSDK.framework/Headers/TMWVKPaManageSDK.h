//
//  TMWVKPaManageSDK.h
//  TMWVKPaManageSDK
//
//  Created by rxk on 2022/1/24.
//  Copyright © 2022 Tianma. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for TMWVKPaManageSDK.
FOUNDATION_EXPORT double TMWVKPaManageSDKVersionNumber;

//! Project version string for TMWVKPaManageSDK.
FOUNDATION_EXPORT const unsigned char TMWVKPaManageSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like
#import <TMWVKPaManageSDK/TMWVKPaWebViewController.h>


